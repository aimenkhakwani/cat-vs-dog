# _Cat vs Dog_

### _A fun jQuery app that pits Cat against Dog, August 9, 2016_

#### _**By Ewa Manek and Aimen Khakwani**_

## Description

_This simple app will finally settle the age old argument of cats vs dogs.  Warning: it's rigged. Also serves as jQuery DOM manipulation practice._

##Setup and Installation

* _Clone from GitHub_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ewa Manek_**
